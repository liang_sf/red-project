const  app = getApp()
Component({
  data: {
    selected: 0,
    color: "#bfbfbf",
    selectedColor: "#1296db",
    list: [{
      pagePath: "/pages/home/index",
      iconPath: "/images/icon_home.png",
      selectedIconPath: "/images/icon_home_HL.png",
      text: "动态"
    }, {
      pagePath: "/pages/talent/index",
      iconPath: "/images/icon_talent.png",
      selectedIconPath: "/images/icon_talent_HL.png",
      text: "网红"
    }, {
      pagePath: "/pages/goodsList/goodsList",
      iconPath: "/images/icon_product.png",
      selectedIconPath: "/images/icon_product_HL.png",
      text: "爆款"
    }, {
      pagePath: "/pages/user/home",
      iconPath: "/images/icon_my.png",
      selectedIconPath: "/images/icon_my_HL.png",
      text: "我的"
    }]
  },
  attached() {
  },
  methods: {
    switchTab(e) {
      const data = e.currentTarget.dataset
      const url = data.path
      wx.switchTab({url})
      this.setData({
        selected: data.index
      })
    }
  }
})