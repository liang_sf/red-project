// pages/home/index.js
const app = getApp()

Page({

  /**
   * 页面的初始数据
   */
  data: {
    page: 0,
    blogList: [],
    lastPage: false
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.getBlogs()
    this.getUserInfo()
  },


  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    this.getTabBar().setData({
      selected: 0  //这个数字是当前页面在tabBar中list数组的索引
    })
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
    let newPage = 0
    this.setData({
      page: newPage,
      blogList: []
    })
    this.getBlogs()
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
    let newPage = this.data.page + 1
    this.setData({
      page: newPage
    })
    this.getBlogs()
  },

  /**
   * 获取blog列表
   */
  getBlogs: function () {
    // const MAX_LIMIT = 2
    // debugger
    // const db = wx.cloud.database()
    // const countResult = db.collection('ht_blogs').count()
    // console.log(countResult)
    // const total = countResult.total
    // const batchTimes = Math.ceil(total / MAX_LIMIT)
    // db.collection('ht_blogs').skip(0 * MAX_LIMIT).limit(MAX_LIMIT).get().then(res => {
    //   res.pages = countResult.total
    //   return res
    // })

    // return false
    if (!this.data.lastPage) {
      wx.showLoading({
        title: '加载中...',
      })
      wx.cloud.callFunction({
        name: 'blog',
        data: {
          action: 'all',
          page: this.data.page
        }
      }).then(res => {
        // console.log(res)
        wx.hideLoading()
        if (res.result.list.length<1) {
          this.setData({
            lastPage: true
          })
        } else {
          this.data.blogList.push(...res.result.list)
          this.setData({
            blogList: this.data.blogList
          })
        }
        
      })
    }
    
  },
  // 跳转到用户主页
  toUserInfo: function (event) {
    console.log(event.currentTarget.dataset.openid)
    
    wx.redirectTo({
      url: '/pages/user/userCard?openid=' + event.currentTarget.dataset.openid,
    })
  },
  
  // 获取用户信息
  getUserInfo: function () {
    wx.cloud.callFunction({
      name: 'users',
      data: {
        action: 'get'
      }
    }).then(res => {
      if (res.result === null) {
        wx.redirectTo({
          url: '../guide/index',
        })
      } else {
        app.globalData.userType = res.result.type
        app.globalData.openid = res.result._openid
        app.globalData.nikeName = res.result.nike_name
        app.globalData.avatarUrl = res.result.avatarUrl
      }
    })
  },
  // 查看图片
  showImg:function (e) {
    console.log(e)
    wx.previewImage({
      urls: this.data.blogList[e.currentTarget.dataset.index].imgs,
      current: e.currentTarget.dataset.imgIndex
    })
  }

})