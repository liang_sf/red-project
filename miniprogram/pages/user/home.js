// pages/user/home.js
const app = getApp()

Page({

  /**
   * 页面的初始数据
   */
  data: {

  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    if (app.globalData.userType == 1) {
      wx.redirectTo({
        url: './user',
      })
    } else {
      wx.redirectTo({
        url: '../enterpriseCenter/enterpriseCenter',
      })
    }
  }

})