// pages/talent/index.js
const app = getApp()

Page({

  /**
   * 页面的初始数据
   */
  data: {
    userList: [],
    page: 0,
    lastPage: false,
    flagList: [],
    currentTag: '',
    keyword: '',
    modalShow: false,
    templateId: 'JJMQvMQMz-VXh2Ea9800j1dX7LSg1ixPFfPfqWduQHY',
    touser: '',
    requestSubscribeMessageResult: false
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.getUserList()
    this.getFlags()
  },
  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
    let newPage = this.data.page + 1
    this.setData({
      page: newPage
    })
    this.getUserList()
  },
  // 获取用户列表
  getUserList: function () {
    
    let where = {
      flag: '',
      nikeName: this.data.keyword,
      type: 1
    }
    if (this.data.currentTag !== '') {
      where.flag = this.data.flagList[this.data.currentTag].name
    }

    if (!this.data.lastPage) {
      wx.showLoading({
        title: '加载中...',
      })

      wx.cloud.callFunction({
        name: 'users',
        data: {
          action: 'all',
          page: this.data.page,
          data: where
        }
      }).then(res => {
        console.log(res)
        wx.hideLoading()
        if (res.result.length<1) {
          this.setData({
            lastPage: true
          })
        } else {
          this.data.userList.push(...res.result)
          this.setData({
            userList: this.data.userList
          })
        }
      })
    } 
  },
  toUserInfo: function (event) {
    wx.redirectTo({
      url: '/pages/user/userCard?openid=' + event.currentTarget.dataset.openid,
    })
  },
  // 查询
  search: function (e) {
    // console.log()
    this.setData({
      keyword: e.detail.value
    })
    this.setData({
      page: 0,
      userList: []
    })
    this.getUserList()
  },
  // 获取所有标签
  getFlags: function () {
    wx.cloud.callFunction({
      name: 'flags',
      data: {
        action: 'get'
      }
    }).then(res => {
      console.log(res)
      this.setData({
        flagList: res.result.data
      })
    })
  },
  // 选择标签
  selectTag: function (e) {
    if (e.currentTarget.dataset.index === this.data.currentTag) {
      this.setData({
        currentTag: ''
      })
    } else {
      this.setData({
        currentTag: e.currentTarget.dataset.index
      })
    }
    this.setData({
      page: 0,
      userList: []
    })
    this.getUserList()
  },
  openSendMsg: function (e) {
    const templateId = this.data.templateId

    if (app.globalData.sendMsgResult) {
      var openid = e.currentTarget.dataset.openid
      this.setData({
        modalShow: true,
        touser: openid
      })
    } else {
      wx.requestSubscribeMessage({
        tmplIds: [templateId],
        success: (res) => {
          if (res[templateId] === 'accept') {
            var openid = e.currentTarget.dataset.openid
            app.globalData.sendMsgResult = true
            this.setData({
              modalShow: true,
              touser: openid
            })
          } else {
            
          }
        },
        fail: (err) => {
          this.setData({
            requestSubscribeMessageResult: `失败（${JSON.stringify(err)}）`,
          })
        },
      })
    }
  },
  closeModal: function () {
    this.setData({
      modalShow:false
    })
  },
  clickSendMsg: function (e) {
    if (!e.detail.value.content) {
      wx.showToast({
        title: '请输入留言内容',
      })
      return false
    }
    wx.cloud.callFunction({
      name: 'openapi',
      data: {
        action: 'sendSubscribeMessage',
        templateId: this.data.templateId,
        openid: this.data.touser,
        name: '留言',
        content: e.detail.value.content
      },
      success: res => {
        console.warn('[云函数] [openapi] subscribeMessage.send 调用成功：', res)
        wx.showToast({
          title: '发送成功',
        })
        this.closeModal()
      },
      fail: err => {
        wx.showToast({
          icon: 'none',
          title: '调用失败',
        })
        console.error('[云函数] [openapi] subscribeMessage.send 调用失败：', err)
      }
    })
  }

})