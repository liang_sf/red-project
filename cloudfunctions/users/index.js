// 云函数入口文件
const cloud = require('wx-server-sdk')

cloud.init()

// 云函数入口函数
exports.main = async (event, context) => {
  const wxContext = cloud.getWXContext()
  switch (event.action) {
    case 'edit': {
        return edit(event, wxContext)
    }
    case 'get': {
        return get(event, wxContext)
    }
    case 'add': {
      return addData(event, wxContext)
    }
    case 'all': {
      return getAllUser(event, wxContext)
    }
  }
  // return {
  //   event,
  //   openid: wxContext.OPENID,
  //   appid: wxContext.APPID,
  //   unionid: wxContext.UNIONID,
  // }
}

const db = cloud.database()
const $ = db.command.aggregate
const MAX_LIMIT = 20


async function edit (event, wxContent) {
  return db.collection('ht_users').doc(event.id).update({
    data: event.data,
    success: function(res) {
      return res
    }
  })
}

async function get (event, wxContent) {
  return db.collection('ht_users').where({
    _openid: event.openid ? event.openid : wxContent.OPENID
  }).get().then(res => {
    return res.data[0]
  })
}

async function addData (event, wxContent) {
  return db.collection('ht_users').add({
    data: {
      ...event.data,
      _openid: wxContent.OPENID,
      add_time: db.serverDate()
    }
  }).then(res => {
    return res
  })
}

/**
 * 获取分页用户
 * @param {} event 
 * @param {*} wxContent 
 */
async function getAllUser (event, wxContent) {

  let where = {}
  if (event.data.nikeName) {
    where.nike_name = event.data.nikeName
  }
  if (event.data.flag) {
    where.flags = event.data.flag
  }
  if (event.data.type == 1) {
    where.type = event.data.type
  }
  return db.collection('ht_users').skip(event.page * MAX_LIMIT).limit(MAX_LIMIT).where(where).get().then(res => {
    return res.data
  })

}